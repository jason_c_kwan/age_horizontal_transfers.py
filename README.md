Age Horizontally Transferred Genes
================================

This repository contains an implementation of a method to estimate the time of introgression for horizontally transferred genes in bacterial genomes. The method was originally published by Lawrence and Ochman in 1997 (1).

If you find this code useful for your research, consider also citing our paper:

Waterworth, S. C.; Flórez, L. V.; Rees, E. R.; Hertweck, C.; Kaltenpoth, M.; Kwan, J. C. [Horizontal gene transfer to a defensive symbiont with a reduced genome in a multipartite beetle microbiome](https://mbio.asm.org/content/11/1/e02430-19). *mBio* **2020**, *11*, e02430-19.

Dependencies
------------
The script is written to run through [Python 2.7](https://www.python.org/downloads/), and requires [Biopython](https://biopython.org/).

USAGE
-----
```bash
age_horizontal_transfers.py --genome_ffn protein_coding.ffn --ht_ffn \
	horizontally_transferred.ffn --synonymous_rate 0.455 \
	--nonsynonymous_rate 0.0245 --tstv_ratio 2 --time_step 0.05 \
	--max_time 500
```

In this example command, we supply a nucleotide fasta (in-frame) of the protein coding genes in the parent genome, minus the horizontally transferred genes (`--genome_ffn`). We also supply a nucleotide fasta containing the sequences of genes we think were horizontally transferred at the same time (`--ht_ffn`). We then supply the synonymous (`--synonymous_rate`) and nonsynonymous (`--nonsynonymous_rate`) substitution rates, as well as the transition/transversion ratio (`--tstv_ratio`). Note that substitution rates are exactly half of the divergence rate between two species, and the units we use are % per million years (be careful about your units of substitution and time - these affect the result). The defaults for these figures (0.455, 0.0245 and 2, respectively) are those calculated for *Escherichia coli* and *Salmonella enterica* in the Lawrence and Ochman paper. We also specify the maximum time to go back to in millions of years when calculating squared deviation sums (see below, `--max_time`), and the step size in millions of years (`--time_step`). 

Explanation
-----------
Lawrence and Ochman derived the following equation:

![Equation 1](https://latex.codecogs.com/gif.latex?\Delta&space;GC^{HT}=S\times&space;\frac{TSTV&space;ratio&space;&plus;&space;\frac{1}{2}}{TSTV&space;ratio&space;&plus;&space;1}&space;\times&space;\left&space;[&space;GC^{EQ}&space;-&space;GC^{HT}\right&space;])

This equation describes the rate of change in GC conent of horizontally transferred DNA (Δ*GC^HT*), in terms of substitution rate (*S*), transition/transversion ratio (*TSTV ratio*) and the GC content of the parent genome (equilibrium GC, *GC^EQ*) and the GC content of the horizontally transferred DNA (*GC^HT*). What this equation is saying is that the rate of change is proportional to the difference in GC between the horizontally transferred fragment and the parent genome. However, because such transferred portions are frequently mostly coding sequences, the rate of amelioration will occur fastest at synonymous sites. In the Lawrence and Ochman paper, substitution rates were derived for the first, second and third positions of codons derived from the known synonymous and nonsynonymous substitution rates in *E. coli* and *S. enterica*. Similarly, in our script we derive these rates from the degree of degeneracy of parent genome codons at each position. 

As formulated above, the equation can be used to determine how long a given set of horizontally transferred genes will take to ameliorate to the host genome. To do this, one would calculate Δ*GC^HT*  using the rates for each codon position (if *S* is in units of % substitution per million years, then this would be equal to the GC change in the next million years), and add these figures to the current GC contents of the horizontally transferred fragment. The number of iterations required to normalize the transferred section to the parent genome would give the time to normalization in millions of years. 

In theory the equation above could be applied in reverse (i.e. by subtracting Δ*GC^HT* instead of adding), to go back in time. A set of horizontally transferred genes would be expected to have GC content characteristic of the donor genome, however this is normally not known. Previously Muto and Osawa (2) determined there are relationships between the GC content at the different codon positions and the GC content of genomes as a whole. These do not hold in horizontally transferred genes because their codon usage does not initially reflect the acceptor genome. Therefore at each iteration of applying the above equation in reverse, Lawrence and Ochman measured the sum of the squared deviations of each codon position in the horizontally transferred genes to the following equations:

![Equation 2](https://latex.codecogs.com/gif.latex?GC_{1st}&space;=&space;0.615&space;\times&space;GC_{genome}&space;&plus;&space;0.269)

![Equation 3](https://latex.codecogs.com/gif.latex?GC_{2st}&space;=&space;0.270&space;\times&space;GC_{genome}&space;&plus;&space;0.267)

![Equation 4](https://latex.codecogs.com/gif.latex?GC_{3rd}&space;=&space;1.692&space;\times&space;GC_{genome}&space;-&space;0.323)

Note that the units of the constants in the equations above differ from those shown in Lawrence and Ochman - here they are expressed as fractions rather than percentages to match the usage in the first equation. As we go through our reverse iterations of the first equation, the iteration number that gives the lowest sum of the squared deviations from the Muto and Osawa equations is equal to the introgression time in millions of years ago. In addition to estimating the time from introgression, this method also estimates the GC content of the donor genome.

Substitution rates to use
-------------------------
The substitution rate is known to vary in bacteria (3), and consequently there is no universal bacterial molecular clock. We were interested in dating horizontal transfers to vertically transmitted symbionts, and previously experimentally determined substitution rates in the aphid symbiont *Buchnera aphidicola* (4) had been used to date the origin of a tsetse fly symbiont (5). However, we found that the synonymous and nonsymonymous substitution rates determined for *B. aphidicola* resulted in converging squared deviations sums, and we could not estimate introgression time. This may be because the extremely relaxed selection in *B. aphidicola* does not impose as much restriction on first and second codon positions as suggested by the Muto and Osawa equations (synonymous/nonsynonymous rate ratio is ~2.9). *B. aphidicola* is an extremely long-term symbiont thought to have infected an ancestor of aphids ~250 Mya, and currently its genome is ~600 kbp in size. Relaxed selection in this setting is thought to be driven by loss of DNA repair pathways and very small effective population size. A recent quantification of dN/dS (time calibrated) in several other symbiont groups suggest that most except for the oldest examples have more skewed synonymous/nonsynonymous substitution rate ratios (6). So, if you are just seeing increases in the sum of squared deviations, it may be because you are using rates with relaxed selection on nonsynonymous sites versus synonymous sites.

Conclusion
----------
This method should be considered as a *rough* estimation of the age of horizontally transferred genes that show detectable differences in GC content compared to their host genome. It will not work with genes that are already ameliorated. The primary application of this method is in situations where phylogeny based methods do not apply. For example in the case of situations where there are an inadequate number of close relatives with the same genes to establish where in the phylogeny the genes were acquired.

References
----------
(1) Lawrence, J. G.; Ochman, H. Amelioration of bacterial genomes: Rates of change and exchange. *Journal of Molecular Evolution* **1997**, *44*, 383-397.

(2) Muto, A.; Osawa, S. The guanine and cytosine content of genomic DNA and bacterial evolution. *Proceedings of the National Academy of Sciences USA* **1987**, *84*, 166-169.

(3) Ochman, H.; Elwyn, S.; Moran, N. A. Calibrating bacterial evolution. *Proceedings of the National Academy of Sciences USA* **1999**, *96*, 12638-12643.

(4) Moran, N. A.; McLaughlin, H.; Sorek, R. The dynamics and time scale of ongoing genomic erosion in symbiotic bacteria. *Science* **2009**, *323*, 379-382.

(5) Clayton, A. L.; Oakeson, K. F.; Gutin, M.; Pontes, A.; Dunn, D. M.; von Niederhausern, A. C.; Weiss, R. B.; Fisher, M.; Dale, C. A novel human-infection-derived bacterium provides insights into the evolutionary origins of mutualistic insect-bacterial symbioses. *PLoS Genetics* **2012**, *8*, e1002990.

(6) Silva, F. J.; Santos-Garcia, D. Slow and fast evolving endosymbiont lineages: Positive correlation between the rates of synonymous and non-synonymous substitution. *Frontiers in Microbiology*, **2015**, *6*, 1279.
