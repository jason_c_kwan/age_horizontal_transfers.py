#!/usr/bin/env python2.7

# Copyright 2019 Jason C. Kwan
#
# age_horizontal_transfers.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# age_horizontal_transfers.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Autometa. If not, see <http://www.gnu.org/licenses/>.

# Program to estimate the age of horizontally transferred genes, using the method proposed by Lawrence and Ochman
# (Journal of Molecular Evolution, 1997, 44, 383-397)

from __future__ import division
import argparse
from Bio import SeqIO
import pprint
from numpy import arange # For float ranges
import pdb
def getDegeneracy(codon, position):
	degeneracy_lookup = { 'TTT': [0,0,0.5], 'TCT': [0,0,1], 'TAT': [0,0,0.5], 'TGT': [0,0,0.5], \
							'TTC': [0,0,0.5], 'TCC': [0,0,1], 'TAC': [0,0,0.5], 'TGC': [0,0,0.5], \
							'TTA': [0.5,0,0.5], 'TCA': [0,0,1], 'TAA': [0,0,0.5], 'TGA': [0,0,0], \
							'TTG': [0.5,0,0.5], 'TCG': [0,0,1], 'TAG': [0,0,0.5], 'TGG': [0,0,0], \
							'CTT': [0,0,1], 'CCT': [0,0,1], 'CAT': [0,0,0.5], 'CGT': [0,0,1], \
							'CTC': [0,0,1], 'CCC': [0,0,1], 'CAC': [0,0,0.5], 'CGC': [0,0,1], \
							'CTA': [0.5,0,1], 'CCA': [0,0,1], 'CAA': [0,0,0.5], 'CGA': [0.5,0,1], \
							'CTG': [0.5,0,1], 'CCG': [0,0,1], 'CAG': [0,0,0.5], 'CGG': [0.5,0,1], \
							'ATT': [0,0,0.75], 'ACT': [0,0,1], 'AAT': [0,0,0.5], 'AGT': [0,0,0.5], \
							'ATC': [0,0,0.75], 'ACC': [0,0,1], 'AAC': [0,0,0.5], 'AGC': [0,0,0.5], \
							'ATA': [0,0,0.75], 'ACA': [0,0,1], 'AAA': [0,0,0.5], 'AGA': [0.5,0,0.5], \
							'ATG': [0,0,0], 'ACG': [0,0,1], 'AAG': [0,0,0.5], 'AGG': [0.5,0,0.5], \
							'GTT': [0,0,1], 'GCT': [0,0,1], 'GAT': [0,0,0.5], 'GGT': [0,0,1], \
							'GTC': [0,0,1], 'GCC': [0,0,1], 'GAC': [0,0,0.5], 'GGC': [0,0,1], \
							'GTA': [0,0,1], 'GCA': [0,0,1], 'GAA': [0,0,0.5], 'GGA': [0,0,1], \
							'GTG': [0,0,1], 'GCG': [0,0,1], 'GAG': [0,0,0.5], 'GGG': [0,0,1] }
	return degeneracy_lookup[codon][position]

# See Table 1 of Lawrence and Ochman
def calculateSubstitutionRates(codon_list):
	rate_list = list()
	for position in [0, 1, 2]:
		degeneracy_total = 0.0
		for codon in codon_list:
			degeneracy_total += getDegeneracy(codon, position)
		synonymous_fraction = degeneracy_total / len(codon_list)
		nonsynonymous_fraction = 1 - synonymous_fraction
		rate_list.append((synonymous_fraction*synonymous_rate) + (nonsynonymous_fraction*nonsynonymous_rate))
	return rate_list

def getGCContent(codon_list):
	gc_contents = list()
	full_gc_codes = ['G', 'C', 'S']
	half_gc_codes = ['R', 'Y', 'K', 'M', 'N']
	two_third_gc_codes = ['B', 'V']
	one_third_gc_codes = ['D', 'H']
	for position in [ 0, 1, 2 ]:
		GC_count = 0
		for codon in codon_list:
			if codon[position] in full_gc_codes:
				GC_count += 1
			elif codon[position] in half_gc_codes:
				GC_count += 0.5
			elif codon[position] in two_third_gc_codes:
				GC_count += 0.667
			elif codon[position] in one_third_gc_codes:
				GC_count += 0.333
		GC_content = GC_count / len(codon_list)
		gc_contents.append(GC_content)
	return gc_contents

def getSquaredDeviationSum(gc_list):
	overall_gc = sum(gc_list) / 3
	calculated_gc_values = list()
	calculated_gc_values.append((0.615*overall_gc) + 0.269)
	calculated_gc_values.append((0.270*overall_gc) + 0.267)
	calculated_gc_values.append((1.692*overall_gc) - 0.323)

	squared_deviation_sum = 0
	for position in [0, 1, 2]:
		deviation = gc_list[position] - calculated_gc_values[position]
		squared_deviation_sum += deviation * deviation

	return squared_deviation_sum

# Checks whether the sequence has symbols not used in the IUPAC nucleotide code (https://www.bioinformatics.org/sms/iupac.html)
def checkSeq(sequence):
	iupac_codes = ['A', 'C', 'G', 'T', 'U', 'R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V', 'N']
	for character in sequence:
		if character not in iupac_codes:
			print('Error! Found sequence the contains a nonvalid nucleotide code')
			exit(1)

parser = argparse.ArgumentParser(description="Estimate the age of horizontally transferred genes in bacterial genomes using the method of Lawrence and Ochman (Journal of Molecular Evolution, 1997, 44,383-397).")
parser.add_argument('-g','--genome_ffn', help='Nucleotide fasta containing coding sequences in frame from host genome (not including horizontally transferred genes).', required=True)
parser.add_argument('-x','--ht_ffn', help='Nucleotide fasta containing coding sequences of horizontally transferred genes, in frame. Program will assume they were introgressed at the same time, so run different sets separately.', required=True)
parser.add_argument('-s','--synonymous_rate', help='Synonymous mutation rate (substitutions per 100 sites per million years). [Default = 0.455]', default=0.455, type=float)
parser.add_argument('-n','--nonsynonymous_rate', help='Nonsynonymous mutation rate (substitutions per 100 sites per million years). [Default = 0.0245]', default = 0.0245, type=float)
parser.add_argument('-t','--tstv_ratio', help='Transition/transversion ratio. [Default = 2.0]', default=2.0, type=float)
parser.add_argument('-e','--time_step', help='Step time in millions of years. [Default = 0.05]', default=0.05, type=float)
parser.add_argument('-m','--max_time', help='Maximum time to go back to, in millions of years. [Default = 500]', default=500, type=float)

args = vars(parser.parse_args())

genome_ffn_path = args['genome_ffn']
ht_ffn_path = args['ht_ffn']
synonymous_rate = args['synonymous_rate']
nonsynonymous_rate = args['nonsynonymous_rate']
tstv_ratio = args['tstv_ratio']
time_step = args['time_step']
max_time = args['max_time']

# Parse genome sequences
genome_codons = list()
for seq_record in SeqIO.parse(genome_ffn_path, 'fasta'):
	seq = str(seq_record.seq).upper()
	checkSeq(seq)
	if len(seq) % 3 != 0:
		seqname = str(seq_record.id)
		print('Error! Sequence ' + seqname + ' in ' + genome_ffn_path + ' does not contain a whole number of codons')
		exit(1)
	codon_list = [ seq[i:i+3] for i in range(0, len(seq), 3) ]
	genome_codons = genome_codons + codon_list

# Parse HT sequences
ht_codons = list()
for seq_record in SeqIO.parse(ht_ffn_path, 'fasta'):
	seq = str(seq_record.seq).upper()
	if len(seq) % 3 != 0:
		seqname = str(seq_record.id)
		print('Error! Sequence ' + seqname + ' in ' + ht_ffn_path + ' does not contain a whole number of codons')
		exit(1)
	codon_list = [ seq[i:i+3] for i in range(0, len(seq), 3) ]
	ht_codons = ht_codons + codon_list

# We work out the proportion of first and third codon sites that are synonymous/nonsynonymous
# and derive the substitution rate at each codon position
codon_substitution_rates = calculateSubstitutionRates(genome_codons)

# Now work out the GC content of the genome and the ht genes at each codon position
genome_gc_contents = getGCContent(genome_codons)
ht_gc_contents = getGCContent(ht_codons)

print('#Input HT genes: ' + ht_ffn_path)
print('#Input genome: ' + genome_ffn_path)
print('#Synonymous substitution rate (% per MY): ' + str(synonymous_rate))
print('#Nonsynonymous substitution rate (% per MY): ' + str(nonsynonymous_rate))
print('#Transition/transversion ratio: ' + str(tstv_ratio))
print('#Time step (MY): ' + str(time_step))
print('#Max time (MY): ' + str(max_time))
print('#')
print('#Genome GC by codon position: ' + ','.join(str(x) for x in genome_gc_contents) + '. Overall: ' + str(sum(genome_gc_contents)/3))
print('#HT GC by codon position: ' + ','.join(str(x) for x in ht_gc_contents) + '. Overall: ' + str(sum(ht_gc_contents)/3))
print('#')

current_ht_gc = ht_gc_contents
squared_deviation_sums = [ getSquaredDeviationSum(current_ht_gc) ]
current_squared_deviation_sum = 0
time_counter = 0
print('Time_Mya\tHT_GC1\tHT_GC2\tHT_GC3\tHT_GCAll\tSum_of_squared_deviations')
print(str(time_counter) + '\t' + str(current_ht_gc[0]) + '\t' + str(current_ht_gc[1]) + '\t' + str(current_ht_gc[2]) + '\t' + str(sum(current_ht_gc)/3) + '\t' + str(squared_deviation_sums[-1]))

for time_counter in arange(time_step, max_time + time_step, time_step):
	new_ht_gc = list()
	for position in [ 0, 1, 2 ]:
		# Note here the rate is substitutions per 100 sites per million years, so to scale for 
		# time_step * million years, we have to multiply the substitution rate accordingly.
		delta_gc = (codon_substitution_rates[position] * time_step) * ((tstv_ratio + 0.5) / (tstv_ratio + 1)) * (genome_gc_contents[position] - current_ht_gc[position])
		new_ht_gc.append(current_ht_gc[position] - delta_gc)
	current_ht_gc = new_ht_gc

	# Do a sanity check, as you can't go below zero GC%
	negative_found = False
	for gc_frac in current_ht_gc:
		if gc_frac < 0:
			negative_found = True

	if negative_found:
		break

	squared_deviation_sums.append(getSquaredDeviationSum(current_ht_gc))
	current_squared_deviation_sum = squared_deviation_sums[-1]
	print(str(time_counter) + '\t' + str(current_ht_gc[0]) + '\t' + str(current_ht_gc[1]) + '\t' + str(current_ht_gc[2]) + '\t' + str(sum(current_ht_gc)/3) + '\t' + str(squared_deviation_sums[-1]))

smallest_deviation = float('inf')
smallest_deviation_index = None

for i,deviation in enumerate(squared_deviation_sums):
	if squared_deviation_sums[i] < smallest_deviation:
		smallest_deviation = squared_deviation_sums[i]
		smallest_deviation_index = i

if i is not None:
	print('#')
	print('#Estimated time of introgression ' + str(smallest_deviation_index * time_step) + ' Mya')